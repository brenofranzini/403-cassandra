package tech.mastertech.itau.cassproj.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cassproj.models.ChavePinUsuario;
import tech.mastertech.itau.cassproj.models.PinUsuario;

public interface PinUsuarioRepository extends CrudRepository<PinUsuario, ChavePinUsuario>{
//	Optional<PinUsuario> findByUsuarioAndDataPost(String usuario, UUID dataPost);
	
	Iterable<PinUsuario> findAllByChavePinUsuario(ChavePinUsuario chavePinUsuario);
}
