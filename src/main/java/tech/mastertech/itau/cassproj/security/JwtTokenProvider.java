package tech.mastertech.itau.cassproj.security;

import java.util.Date;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenProvider {
	private final int duracaoEmMilisegundos = 10000000;
	private final String chaveSecreta = "aVerySecretKeyOfParanaue";

	public String criarToken(String userId) {
		// claims é o payload
		Claims claims = Jwts.claims();

		claims.put("userId", userId);

		Date agora = new Date();
		Date expiracao = new Date(agora.getTime() + duracaoEmMilisegundos);

		return Jwts.builder().setClaims(claims).setExpiration(expiracao)
				.signWith(SignatureAlgorithm.HS256, chaveSecreta).compact();
	}

	public boolean validarToken(String token) {
		try {
			Jwts.parser().setSigningKey(chaveSecreta).parseClaimsJws(token);

			return true;

		} catch (Exception e) {
			return false;
		}
	}

	public String lerToken(String token) {
		Jws<Claims> jwsClaims = Jwts.parser().setSigningKey(chaveSecreta).parseClaimsJws(token);

		return jwsClaims.getBody().get("userId", String.class);
	}
}
