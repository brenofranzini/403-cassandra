package tech.mastertech.itau.cassproj.models;

import javax.validation.constraints.NotNull;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class PinCategoria {

	@PrimaryKey
	private ChavePinCategoria chavePinCategoria;

	@NotNull
	private String imagem;
	private String descricao;
	@NotNull
	private String usuario;
	
	public ChavePinCategoria getChavePinCategoria() {
		return chavePinCategoria;
	}
	public void setChavePinCategoria(ChavePinCategoria chavePinCategoria) {
		this.chavePinCategoria = chavePinCategoria;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}