package tech.mastertech.itau.cassproj.models;

import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

@PrimaryKeyClass
public class ChavePinFavorito {
	@PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
	private UUID dataPost;

	@PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
	private String postador;

	@PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
	private UUID dataFav;

	@PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
	private String usuario;

	public UUID getDataPost() {
		return dataPost;
	}

	public void setDataPost(UUID dataPost) {
		this.dataPost = dataPost;
	}

	public String getPostador() {
		return postador;
	}

	public void setPostador(String postador) {
		this.postador = postador;
	}

	public UUID getDataFav() {
		return dataFav;
	}

	public void setDataFav(UUID dataFav) {
		this.dataFav = dataFav;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}