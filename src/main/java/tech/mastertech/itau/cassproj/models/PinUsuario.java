package tech.mastertech.itau.cassproj.models;

import javax.validation.constraints.NotNull;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class PinUsuario {
	
	@PrimaryKey
	private ChavePinUsuario chavePinUsuario;
	
	@NotNull
	private String imagem;
	private String descricao;
	@NotNull
	private String categoria;

	public ChavePinUsuario getChavePinUsuario() {
		return chavePinUsuario;
	}

	public void setChavePinUsuario(ChavePinUsuario chavePinUsuario) {
		this.chavePinUsuario = chavePinUsuario;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
}