package tech.mastertech.itau.cassproj.models;

public interface DadosBasicos {
	String getNome();
	String getFotoPerfil();
}