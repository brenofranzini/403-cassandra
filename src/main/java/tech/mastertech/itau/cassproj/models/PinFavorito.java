package tech.mastertech.itau.cassproj.models;

import javax.validation.constraints.NotNull;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class PinFavorito {

	@PrimaryKey
	private ChavePinFavorito chavePinFavorito;

	@NotNull
	private String imagem;
	private String descricao;
	@NotNull
	private String categoria;

	public ChavePinFavorito getChavePinFavorito() {
		return chavePinFavorito;
	}

	public void setChavePinFavorito(ChavePinFavorito chavePinFavorito) {
		this.chavePinFavorito = chavePinFavorito;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
}