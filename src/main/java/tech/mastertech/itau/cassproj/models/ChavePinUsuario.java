package tech.mastertech.itau.cassproj.models;

import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

@PrimaryKeyClass
public class ChavePinUsuario {
	
	@PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
	private UUID dataPost;

	@PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
	private String usuario;

	public UUID getDataPost() {
		return dataPost;
	}

	public void setDataPost(UUID dataPost) {
		this.dataPost = dataPost;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}