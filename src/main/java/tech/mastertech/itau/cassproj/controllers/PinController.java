package tech.mastertech.itau.cassproj.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.cassproj.dtos.PIN;
import tech.mastertech.itau.cassproj.models.PinUsuario;
//import tech.mastertech.itau.cassproj.models.PinUsuario;
import tech.mastertech.itau.cassproj.services.PinService;

@RestController
@RequestMapping("/pin")
public class PinController {

	@Autowired
	private PinService pinService;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void cadastrarPIN(@RequestBody PIN pin) {
		pinService.cadastrarPIN(pin);
	}
	
	@GetMapping("/usuario/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Iterable<PinUsuario> getPinPorUsuario(@RequestParam(required = false) String id) {
		return pinService.getPinUsuario(id);
	}
	
//	@GetMapping("/pin/categoria/{id}")
//	@ResponseStatus(HttpStatus.CREATED)
//	public void getPinPorCategoria(@RequestParam(required = false) String id) {
//		pinService.getPinCategoria(id);
//	}
}
