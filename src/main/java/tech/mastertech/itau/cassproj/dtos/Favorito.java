package tech.mastertech.itau.cassproj.dtos;

import java.util.UUID;

import javax.validation.constraints.NotBlank;

public class Favorito {
	@NotBlank
	private String usuario;
	@NotBlank
	private UUID dataPost;
	@NotBlank
	private String postador;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public UUID getDataPost() {
		return dataPost;
	}

	public void setDataPost(UUID dataPost) {
		this.dataPost = dataPost;
	}

	public String getPostador() {
		return postador;
	}

	public void setPostador(String postador) {
		this.postador = postador;
	}
}