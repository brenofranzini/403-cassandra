package tech.mastertech.itau.cassproj.services;

//import java.util.Optional;
import java.util.UUID;

//import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datastax.driver.core.utils.UUIDs;

import tech.mastertech.itau.cassproj.dtos.PIN;
import tech.mastertech.itau.cassproj.models.ChavePinCategoria;
import tech.mastertech.itau.cassproj.models.ChavePinUsuario;
import tech.mastertech.itau.cassproj.models.PinCategoria;
import tech.mastertech.itau.cassproj.models.PinUsuario;
import tech.mastertech.itau.cassproj.repositories.PinCategoriaRepository;
//import tech.mastertech.itau.cassproj.repositories.PinFavoritoRepository;
import tech.mastertech.itau.cassproj.repositories.PinUsuarioRepository;

@Service
public class PinService {

//	private ModelMapper mapper = new ModelMapper();

	@Autowired
	private PinUsuarioRepository pinUsuario;

	@Autowired
	private PinCategoriaRepository pinCategoria;

//	@Autowired
//	private PinFavoritoRepository pinFavorito;

	@Transactional(rollbackFor = Exception.class)
	public void cadastrarPIN(PIN pin) {
		UUID data = UUIDs.timeBased();
		pin.setDataPost(data);
		
//		ChavePinUsuario chaveUsuario = new ChavePinUsuario();
//		chaveUsuario.setUsuario(pin.getUsuario());

//		Optional<PinUsuario> pinUsuarioOptional = pinUsuario.findAllByChavePinUsuario(chaveUsuario);
//		Optional<PinCategoria> pinCategoriaOptional = pinCategoria.findById(pin.getCategoria());

//		if (!pinUsuarioOptional.isPresent() && !pinCategoriaOptional.isPresent()) {
			PinUsuario pUsuario = new PinUsuario();
			PinCategoria pCategoria = new PinCategoria();
			
			ChavePinUsuario chaveUsuario = new ChavePinUsuario();
			ChavePinCategoria chaveCategoria = new ChavePinCategoria();
			
			chaveUsuario.setDataPost(pin.getDataPost());
			chaveUsuario.setUsuario(pin.getUsuario());
			
			chaveCategoria.setCategoria(pin.getCategoria());
			chaveCategoria.setDataPost(pin.getDataPost());
			
			
			pUsuario.setCategoria(pin.getCategoria());
			pUsuario.setDescricao(pin.getDescricao());
			pUsuario.setImagem(pin.getImagem());
			pUsuario.setChavePinUsuario(chaveUsuario);
			
			pCategoria.setDescricao(pin.getDescricao());
			pCategoria.setImagem(pin.getImagem());
			pCategoria.setUsuario(pin.getUsuario());
			pCategoria.setChavePinCategoria(chaveCategoria);
			
			pinUsuario.save(pUsuario);
			pinCategoria.save(pCategoria);
//		}
	}

	public Iterable<PinUsuario> getPinUsuario(String usuario) {
		Iterable<PinUsuario> pinUsuarioIterable;
		
		ChavePinUsuario chave = new ChavePinUsuario();
		chave.setUsuario(usuario);
		
		if (usuario == null) {
			pinUsuarioIterable = pinUsuario.findAllByChavePinUsuario(chave);
		}
		else {
			pinUsuarioIterable = pinUsuario.findAll();
		}

		return pinUsuarioIterable;
	}

//	public Iterable<PinCategoria> getPinCategoria(String categoria) {
//		Iterable<PinCategoria> pinCategoriaIterable;
//		if (categoria == null) {
//			pinCategoriaIterable = pinCategoria.findAllByCategoria(categoria);
//		} else {
//			pinCategoriaIterable = pinCategoria.findAll();
//		}
//		return pinCategoriaIterable;
//	}

//	public void favoritarPin(Favorito favorito) {
//		UUID dataFav = UUIDs.timeBased();
//
//		Optional<PinUsuario> pinUsuarioOptional = pinUsuario.findByUsuarioAndDataPost(favorito.getPostador(),
//				favorito.getDataPost());
//
//		if (pinUsuarioOptional.isPresent()) {
//			PinUsuario pin = pinUsuarioOptional.get();
//			PinFavorito pFavorito = new PinFavorito();
//
//			pFavorito.setCategoria(pin.getCategoria());
//			pFavorito.getChavePinFavorito().setDataFav(dataFav);
//			pFavorito.getChavePinFavorito().setDataPost(pin.getChavePinUsuario().getDataPost());
//			pFavorito.setDescricao(pin.getDescricao());
//			pFavorito.setImagem(pin.getImagem());
//			pFavorito.getChavePinFavorito().setPostador(pin.getChavePinUsuario().getUsuario());
//			pFavorito.getChavePinFavorito().setUsuario(favorito.getUsuario());
//
//			pinFavorito.save(pFavorito);
//		}
//
//		throw new ResponseStatusException(HttpStatus.NOT_FOUND);
//	}
}
